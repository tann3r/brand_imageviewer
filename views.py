from flask import abort, jsonify, render_template, request, send_file
from PIL import Image, ImageOps, ImageDraw
import os
import tempfile
from StringIO import StringIO
from pprint import pprint
from werkzeug.contrib.cache import SimpleCache

from app import app, APP_ROOT
from models import ProductInfo, ProductBrand, ProductIdName

SIZE = (150, 150)

cache = SimpleCache(threshold=1000, default_timeout=1200)

def make_thumbnail(img_path, size):
    tmpfile_path = ''
    if img_path:
        try:
            with open(str(os.path.join(APP_ROOT, 'static', img_path)), 'rb') as infile:
                image = Image.open(infile)
                image.thumbnail(size, Image.ANTIALIAS)
                background = Image.new('RGBA', size, (255, 255, 255, 0))
                background.paste(
                    image,
                    ((size[0] - image.size[0]) / 2, (size[1] - image.size[1]) / 2))
                fd, tmpfile_path = tempfile.mkstemp(prefix=img_path, dir=os.path.join(APP_ROOT, 'static', 'thumbnails'))
                outsock = os.fdopen(fd, 'w')
                background.save(outsock, image.format, quality=70)
                outsock.close()
                del image, background

        except IOError as e:
            app.logger.info('%s is missing' % img_path)
            background = Image.new('RGBA', size, (255, 255, 255, 0))
            draw = ImageDraw.Draw(background)
            text_w, text_h = draw.textsize('X')
            draw.text(
                ((SIZE[0] - text_w)/2, (SIZE[1] - text_h)/2),
                'X',
                (0, 0, 0)
            )
            fd, tmpfile_path = tempfile.mkstemp(prefix=img_path, dir=os.path.join(APP_ROOT, 'static', 'thumbnails'))
            outsock = os.fdopen(fd, 'w')
            background.save(outsock, 'PNG', quality=70)
            outsock.close()
            del draw, background

        return tmpfile_path.replace(APP_ROOT, '')

@app.route('/imageviewer')
def index():
    brands_dict = ProductBrand.get_brands()
    brands_tuples_list = []
    for index in sorted(brands_dict, key=brands_dict.get):
        brands_tuples_list.append((index, brands_dict[index]))

    # create thumbnails dir if it's missing
    thumbnails_path = os.path.join(APP_ROOT, 'static', 'thumbnails')
    if not os.path.isdir(thumbnails_path):
        os.makedirs(thumbnails_path)

    else:
        # delete all prev files in thumbnails dir if there were any
        # dirty hack
        if os.listdir(thumbnails_path):
            for path, subdirs, files in os.walk(thumbnails_path):
                for name in files:
                    os.remove(os.path.join(path, name))

    return render_template('viewer.html', brands_tuples_list=brands_tuples_list)

@app.route('/_get_prods_by_id')
def get_prods_by_id():
    products_list = ProductIdName.get_products_by_brand_id(request.args.get('product_id', 0, type=int))
    brand_name = ProductBrand.get_brand_name_by_id(request.args.get('product_id', 0, type=int))

    # update img path from db to thumbnails
    for item in products_list:
        thumb_img_items = []
        for img_item in item['imgs']:
            img_path, imgTypeCd = img_item


            if not os.path.isdir(os.path.split(os.path.join(APP_ROOT, 'static', 'thumbnails', img_path))[0]):
                print os.path.split(os.path.join(APP_ROOT, 'static', 'thumbnails', img_path))[0]
                os.makedirs(os.path.split(os.path.join(APP_ROOT, 'static', 'thumbnails', img_path))[0])

            if cache.get(img_path):
                thumb_img_items.append(
                    (
                        cache.get(img_path),
                        imgTypeCd
                    )
                )
            else:
                thumb_img_path = make_thumbnail(img_path, SIZE)
                cache.set(img_path, thumb_img_path)
                thumb_img_items.append(
                    (
                        thumb_img_path,
                        imgTypeCd
                    )
                )
        item['imgs'] = thumb_img_items

    return jsonify(products_list=products_list, brand_name=brand_name)
