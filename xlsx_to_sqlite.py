import xlrd
from pprint import pprint
from peewee import *
import datetime
import logging

DATABASE = 'prdimgraw.db'
XLSFILENAME = 'docs/PrdImgRaw.2016.01.26.xlsx'

database = SqliteDatabase(DATABASE, threadlocals=True)

# setting logger
logger = logging.getLogger('xlsx_to_sqlite')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

ch.setFormatter(formatter)

logger.addHandler(ch)


class BaseModel(Model):
    class Meta:
        database = database

class ProductBrand(BaseModel):
    prdBrand_id = PrimaryKeyField()
    prdBrand = CharField(
        unique=True
    )

    class Meta:
        db_table = 'ProductBrand'

class ProductIdName(BaseModel):
    prdID_id = PrimaryKeyField()
    prdID = CharField(unique=True)
    prdName = CharField(null=True)
    prdBrand = ForeignKeyField(ProductBrand, to_field='prdBrand_id')

    class Meta:
        db_table = 'ProductIdName'

class ProductInfo(BaseModel):
    prdID = ForeignKeyField(ProductIdName, to_field='prdID_id')
    prdSource = CharField(null=True)
    prdUrl = CharField(null=True)
    imgNameRaw = CharField(null=True)
    imgNameRefined = CharField(null=True)
    imgPath = CharField(unique=True, null=True)
    imgDimensions = CharField(null=True)
    imgWidth = CharField(null=True)
    imgHeight = CharField(null=True)
    imgHoriz = CharField(null=True)
    imgVert = CharField(null=True)
    imgBitDepth = CharField(null=True)
    imgType = CharField(null=True)
    bBarCode = CharField(null=True)
    bQRC = CharField(null=True)
    imgTypeCd = CharField(null=True)
    imgBatch = CharField(null=True)
    imgBatchDt = DateField(null=True)
    imgTransCd = CharField(null=True)

    class Meta:
        db_table = 'ProductInfo'

if __name__ == '__main__':
    book = xlrd.open_workbook(XLSFILENAME)
    sheet = book.sheet_by_index(0)

    database.connect()

    try:
        database.create_tables([ProductBrand, ProductInfo, ProductIdName])
    except OperationalError:
        logger.info('tables already exist')


    column_names = []
    for rownum in range(sheet.nrows):
        if rownum == 0:
            for cell_value in sheet.row_values(rownum):
                column_names.append(cell_value)
        else:
            try:
                product_brand_obj = ProductBrand.create(prdBrand=sheet.row_values(rownum)[3])
            except IntegrityError:
                # logger.info('%s product brand already in the database, skipping' % (sheet.row_values(rownum)[3]))
                pass

            try:
                product_id_obj = ProductIdName.create(
                    prdID=sheet.row_values(rownum)[0],
                    prdName=sheet.row_values(rownum)[4],
                    prdBrand=ProductBrand.select().where(ProductBrand.prdBrand == sheet.row_values(rownum)[3]).get().get_id())
            except IntegrityError:
                logger.info('%s product id already in the database, skipping' % (sheet.row_values(rownum)[0]))

            product_dict = {}
            for index, cell_value in enumerate(sheet.row_values(rownum)):
                if column_names[index] == 'imgBatchDt':
                    try:
                        product_dict[column_names[index]] = datetime.datetime(*xlrd.xldate_as_tuple(cell_value, book.datemode)).strftime('%Y-%m-%d')
                    except xlrd.xldate.XLDateAmbiguous:
                        product_dict[column_names[index]] = None

                elif column_names[index] == 'prdID':
                    product_dict[column_names[index]] = ProductIdName.select().where(ProductIdName.prdID == cell_value).get().get_id()


                else:
                    # xlrd reads numbers from xls in float
                    # if number is int, write as int, else as float
                    # value error for string values
                    try:
                        product_dict[column_names[index]] = int(cell_value) if cell_value \
                            and (int(cell_value) == cell_value) \
                            else cell_value
                    except ValueError:
                        product_dict[column_names[index]] = cell_value

            try:
                product_info_obj = ProductInfo.create(**product_dict)
                logger.info('%s added' % sheet.row_values(rownum)[0])
            except (IntegrityError) as e:
                logger.info('%s already in database' % product_dict.get('imgPath', None))

    database.close()
