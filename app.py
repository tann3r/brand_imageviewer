import os

from flask import Flask, url_for, render_template
from peewee import SqliteDatabase


APP_ROOT = os.path.join(os.path.dirname(os.path.realpath(__file__)), '')
DATABASE = os.path.join(APP_ROOT, 'prdimgraw.db')
# DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)

db = SqliteDatabase(app.config['DATABASE'], threadlocals=True)
