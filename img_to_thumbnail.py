from PIL import Image, ImageOps
from pprint import pprint

# from os import listdir
# from os.path import isfile, join
import os

IMGS_ROOT_DIR = '/home/tanner/devel/brand_imageviewer/static/images'
THUMBS_ROOT_DIR = '/home/tanner/devel/brand_imageviewer/static/thumbnails'
SIZE = (150, 300)

#dirty hack for adding trailing slashes
IMGS_ROOT_DIR = os.path.join(IMGS_ROOT_DIR, '')
THUMBS_ROOT_DIR = os.path.join(THUMBS_ROOT_DIR, '')

def make_thumbnail(size, old_path, new_path):
    try:
        image = Image.open(old_path)
        image.thumbnail(size, Image.ANTIALIAS)
        background = Image.new('RGBA', size, (255, 255, 255, 0))
        background.paste(
            image,
            ((size[0] - image.size[0]) / 2, (size[1] - image.size[1]) / 2))

        background.save(new_path)

        print '%s done' % (new_path)

    except IOError as e:
        print e, old_path

if __name__ == '__main__':
    if os.path.isdir(IMGS_ROOT_DIR):
        if not os.path.isdir(THUMBS_ROOT_DIR):
            os.mkdir(THUMBS_ROOT_DIR)

        for path, subdirs, files in os.walk(IMGS_ROOT_DIR):
            for name in files:
                orig_img_path = os.path.join(path, name)
                thumb_img_path = os.path.join(
                    THUMBS_ROOT_DIR,
                    os.path.basename(os.path.normpath(IMGS_ROOT_DIR)),
                    orig_img_path.replace(IMGS_ROOT_DIR, '')
                )

                if not os.path.isdir(os.path.dirname(thumb_img_path)):
                    os.makedirs(os.path.dirname(thumb_img_path))

                make_thumbnail(SIZE, orig_img_path, thumb_img_path)

    else:
        print IMGS_ROOT_DIR, 'is not a dir'


# for pa in img_paths:s

        # img_files.append(os.path.join(path, name))
        # pa = os.path.join(path, name)
        # pa = pa.replace(IMGS_ROOT_DIR, '')
        # # print os.path.normpath(pa)
        # print os.path.join(THUMBS_ROOT_DIR, pa)

# print os.path.basename(os.path.normpath(IMGS_ROOT_DIR))
# pprint(imgs)


# image = Image.open('/home/tanner/devel/brand_imageviewer/static/images/Bebi/10589174121d3d4a00fb028e83af2820_000000.jpg')
# size = (200, 200)






