import datetime

from app import db
from peewee import *
from pprint import pprint
from playhouse.shortcuts import model_to_dict

class BaseModel(Model):
    class Meta:
        database = db

class ProductBrand(BaseModel):
    prdBrand_id = PrimaryKeyField()
    prdBrand = CharField(
        unique=True
    )

    class Meta:
        db_table = 'ProductBrand'

    @classmethod
    def get_brands(cls):
        brands = {}
        for item in ProductBrand.select().order_by(-ProductBrand.prdBrand):
            if item.prdBrand not in brands.values():
                brands[item.get_id()] = item.prdBrand

        return brands

    @classmethod
    def get_brand_name_by_id(cls, brand_id):
        brand_id_obj = ProductBrand.select().where(ProductBrand.prdBrand_id == brand_id).get()

        return brand_id_obj.prdBrand

class ProductIdName(BaseModel):
    prdID_id = PrimaryKeyField()
    prdID = CharField(unique=True)
    prdName = CharField(null=True)
    prdBrand = ForeignKeyField(ProductBrand, to_field='prdBrand_id')

    class Meta:
        db_table = 'ProductIdName'

    @classmethod
    def get_products_by_brand_id(cls, brand_id):
        products = []

        products_items = ProductIdName.select().where(ProductIdName.prdBrand == brand_id)

        for item in products_items:
            products_info_items = ProductInfo.select().where(ProductInfo.prdID == item.prdID_id)
            imgs = []
            for prdinfitem in products_info_items:
                imgs.append((prdinfitem.imgPath, prdinfitem.imgTypeCd))

            products.append({'prdName': item.prdName, 'imgs': imgs})

        return products




class ProductInfo(BaseModel):
    prdID = ForeignKeyField(ProductIdName, to_field='prdID_id')
    prdSource = CharField(null=True)
    prdUrl = CharField(null=True)
    imgNameRaw = CharField(null=True)
    imgNameRefined = CharField(null=True)
    imgPath = CharField(unique=True, null=True)
    imgDimensions = CharField(null=True)
    imgWidth = CharField(null=True)
    imgHeight = CharField(null=True)
    imgHoriz = CharField(null=True)
    imgVert = CharField(null=True)
    imgBitDepth = CharField(null=True)
    imgType = CharField(null=True)
    bBarCode = CharField(null=True)
    bQRC = CharField(null=True)
    imgTypeCd = CharField(null=True)
    imgBatch = CharField(null=True)
    imgBatchDt = DateField(null=True)
    imgTransCd = CharField(null=True)

    class Meta:
        db_table = 'ProductInfo'
